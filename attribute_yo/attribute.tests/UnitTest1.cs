﻿using NUnit.Framework;

namespace attribute.tests
{
    [TestFixture]
    public class UnitTest1
    {
        private readonly SomeService service = new SomeService();

        [Test]
        public void DoNoParams()
        {
            Helper.DoTheThing(service,nameof(service.DoStuff));
        }


        [Test]
        public void DoParam()
        {
            Helper.DoTheThing(service,nameof(service.DoStuff));

            var result = Helper.DoTheThing(service, nameof(service.DoOtherStuff), 123, "abc");

            Assert.NotNull(result);
            var trans = result as int?;
            Assert.NotNull(trans);

            Assert.AreEqual(123, trans.Value);

        }

        [Test]
        public void DoThrow()
        {
            Assert.Throws<AccessDENIED>(() => Helper.DoTheThing(service, nameof(service.CantDo)));
        }

        [Test]
        public void NoAttribute()
        {
            var val = Helper.DoTheThing(service, nameof(service.NoAttribute), 12345);
            Assert.NotNull(val);
            var unpack = val as int?;
            Assert.NotNull(unpack);
            Assert.AreEqual(unpack, 12345);

        }
    }
}
