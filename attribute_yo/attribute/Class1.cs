﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attribute
{
    public enum AttributeEnum
    {
        Undefined,
        CanDoStuff,
        CantDoStuff
    }
    public class SuperCoolAttribute : Attribute
    {
        private readonly AttributeEnum attribute;

        public SuperCoolAttribute(AttributeEnum attribute)
        {
            this.attribute = attribute;
        }

        public AttributeEnum AttributeEnum => this.attribute;
    }

    public class SomeService
    {
        [SuperCool(AttributeEnum.CanDoStuff)]
        public void DoStuff()
        {
            Console.WriteLine("I do stuff");
        }
        [SuperCool(AttributeEnum.CanDoStuff)]
        public int DoOtherStuff(int number, string str)
        {
            Console.WriteLine($"I do stuff: {number}{str}" );
            return number;
        }

        [SuperCool(AttributeEnum.CanDoStuff)]
        public int Count()
        {
            return 1234;
        }

        [SuperCool(AttributeEnum.CantDoStuff)]
        public void CantDo()
        {
            Console.WriteLine("I shouldnt be doing anything");
        }

        public int NoAttribute(int number)
        {
            return number;
        }

    }

    public class AccessDENIED : Exception
    {

    }
}
