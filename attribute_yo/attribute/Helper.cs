﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace attribute
{
    public class Helper
    {
        public static object DoTheThing<T>(T obj, String str, params object[] stuff)
        {
            var method = typeof(T).GetMethod(str);
            if (method != null)
            {
                var customs = method.GetCustomAttributes(typeof(SuperCoolAttribute), true);
                if (customs.Any())
                {
                    var attr = customs.First() as SuperCoolAttribute;
                    if (attr?.AttributeEnum != AttributeEnum.CanDoStuff)
                    {
                        throw new AccessDENIED();
                    }

                }
            }
            return method.Invoke(obj, stuff);
        }
    }
}
